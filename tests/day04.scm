(define-module (advent2023 test-day04))

(use-modules (srfi srfi-64)
             (advent2023 day04)
             (ice-9 textual-ports))

(define *input* (call-with-input-file "./inputs/day04-input1.txt" get-string-all))

(define *input-ex* "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11")

(solution-2 "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11")

(test-begin "day04")

(test-eqv 13
  (solution-1 *input-ex*))

(test-eqv 23028
  (solution-1 *input*))

(test-eqv 30
  (solution-2 *input-ex*))

(test-eqv 9236992
  (solution-2 *input*))

(test-end "day04")
