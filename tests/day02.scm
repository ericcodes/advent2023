(define-module (advent2023 test-day02))

(use-modules (srfi srfi-64)
             (advent2023 day02)
             (ice-9 textual-ports))

(define *sack-1* (make-sack 12 13 14))

(define *input* (call-with-input-file "./inputs/day02-input1.txt" get-string-all))

(define *input-ex* "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green")

(test-begin "day02")

(test-eqv 8
  (solution-1 *sack-1* *input-ex*))

(test-eqv 2317
  (solution-1 *sack-1* *input*))

(test-eqv 2286
  (solution-2 *input-ex*))

(test-eq 74804
  (solution-2 *input*))

(test-end "day02")
