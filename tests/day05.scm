(define-module (advent2023 test-day05))

(use-modules (srfi srfi-64)
             (advent2023 day05)
             (ice-9 textual-ports))


(day05-tests)
