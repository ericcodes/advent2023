(define-module (advent2023 test-day01))

(use-modules (srfi srfi-64)
             (advent2023 day01)
             (ice-9 textual-ports))

(define *input-ex* "1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet")

(define *input-ex2*
  "two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen")

(define *input* (call-with-input-file "./inputs/day01-input1.txt" get-string-all))

(test-begin "day01")

(test-eqv 142
  (solution-1 *input-ex*))

(test-eqv 281
  (solution-2 *input-ex2*))

(test-eqv 52974
  (solution-1 *input*))

(test-eqv 53340
  (solution-2 *input*))

(test-end "day01")
