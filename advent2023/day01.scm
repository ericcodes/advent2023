;; I initially used a PEG parser for this puzzle.  This solution was
;; mostly just an excuse to learn how PEG parsing works.
;;
;; Solution one was able to use the calibration document (caldoc) PEG
;; parser as is.
;;
;; However when I got to the second puzzle, I couldn't use a PEG
;; parser for the entire document because the left and right most
;; digits could overlap. For instance the pharse "twone" equates to
;; 21. A more complex example could be "1xdfetwone" which would equate
;; to 11.
;;
;; Initially, I simply added the `word` parser to the `entry` parser,
;; but because the PEG parser scans left to right, "twone" would only
;; find the first "two" substring and it would incorrectly evaluate to
;; 22.
;;
;; The solution I came up with was to define the a new `part` parser
;; and scan the line from the left until it matched and scan the line
;; from the right until it matched. Then I convert those matches into a
;; two digit number.
;;
;; The scanning code isn't very efficient but it gets the correct
;; answer.
(define-module (advent2023 day01)
  #:export (solution-1 solution-2 *input-ex* *input-ex2*))

(use-modules (ice-9 peg))

(use-modules (ice-9 match))

(use-modules (ice-9 textual-ports))

(use-modules (srfi srfi-1))

(define-peg-string-patterns
  "caldoc <- entry* !.
entry <-- (text* digit?)* NL*
text < (!digit !NL .)*

part <- (text-2* (word/digit) text-2*)
text-2 < (!digit !word !NL .)*

digit <-- [0-9]
word <-- 'one'/'two'/'three'/'four'/'five'/'six'/'seven'/'eight'/'nine'
NL < '\n'")

;; Use pattern matching on the tagged PEG parser results for the part parser.
;;
;; This is purposefully non-exhaustive so that it'll crash if my parser is incorrect.
(define (part->number word)
  (match word
    ('(word "one") 1)
    ('(word "two") 2)
    ('(word "three") 3)
    ('(word "four") 4)
    ('(word "five") 5)
    ('(word "six") 6)
    ('(word "seven") 7)
    ('(word "eight") 8)
    ('(word "nine") 9)
    (`(digit (cg-range ,digit)) (string->number digit))))

;; entry->sum is used by the first solution
;;
;; The entry parser returns a list that looks like the list:
;;
;; `(entry (word "one") (digit "1") (word "two"))`
;;
;; It is pretty self explanatory. I take the first item from parts and the
;; final pair from parts and sum them together
(define (entry->sum entry-tag . parts)
  (define left (part->number (car parts)))
  (define right (part->number (car (last-pair parts))))

  (+ (* left 10) right))

;; solution-1 uses the caldoc parser to build a parsed tree
;;
;; The tree looks something like this:
;;
;; ((entry (word "one") (digit "2"))
;;  (entry (digit "4") (word "four")))
;;
;; I then evaluate that tree into a total sum using the `entry->sum`
;; function.
(define (solution-1 input)
  (define tree
    (peg:tree (match-pattern caldoc input)))

  (fold (lambda (entry total) (+ total (apply entry->sum entry)))
        0
        tree))

;; solution-2 has to do things a little different because a line could
;; have the phrase "twone" or "eightwo". We have to scan from the left
;; to get the left most digit and scan from the right to get the right
;; most digit.
;;
;; This is not the most efficient because we're splitting into lines,
;; then mapping over each line but whatever. Finding this solution was
;; a PITA because they threw a curve ball by allowing the left and
;; right digits to overlap.
(define (solution-2 input)
  (define lines (string-split input #\newline))
  (fold (lambda (line total)
          (+ total (line->sum line)))
        0
        lines))

;; line->sum uses line->left and line->right to find the leftmost and
;; rightmost digits using the part parser.
(define (line->sum line)
  (if (string-null? line)
      0
      (let ((left (part->number (peg:tree (line->left line))))
            (right (part->number (peg:tree (line->right line)))))
        (+ (* left 10) right))))

;; line->left scans the line left to right until the part parser passes
(define (line->left line)
  (define found #f)
  (do
      ((i 0 (1+ i)))
      ((or found (> i (string-length line))) found)
    (set! found (match-pattern part (substring line 0 i)))))

;; line->right scans the line right to left until the part parser passes
(define (line->right line)
  (define found #f)
  (define len (string-length line))

  (do
      ((i len (1- i)))
      ((or found (< i 0)) found)
    (set! found (match-pattern part (substring line i len)))))
