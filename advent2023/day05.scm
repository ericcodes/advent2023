(define-module (advent2023 day05)
  #:export (day05-tests))

(use-modules (ice-9 peg)
             (ice-9 match)
             (ice-9 format)
             (ice-9 pretty-print)
             (srfi srfi-26)
             (srfi srfi-11)
             (srfi srfi-1)
             (srfi srfi-9)
             (srfi srfi-64)
             (advent2023 day05)
             (ice-9 textual-ports))


;; This is the PEG grammar for the input file
(define-peg-string-patterns
  "NL < '\n'
 S < ' '

seeds-label < 'seeds: '
seeds <-- seeds-label int+

int <-- [0-9]+ S*

seed-to-soil-label < 'seed-to-soil map:\n'
seed-to-soil-map <-- seed-to-soil-label almanac-map+

soil-to-fertilizer-label < 'soil-to-fertilizer map:\n'
soil-to-fertilizer-map <-- soil-to-fertilizer-label almanac-map+

fertilizer-to-water-label < 'fertilizer-to-water map:\n'
fertilizer-to-water-map <-- fertilizer-to-water-label almanac-map+

water-to-light-label < 'water-to-light map:\n'
water-to-light-map <-- water-to-light-label almanac-map+

light-to-temperature-label < 'light-to-temperature map:\n'
light-to-temperature-map <-- light-to-temperature-label almanac-map+

temperature-to-humidity-label < 'temperature-to-humidity map:\n'
temperature-to-humidity-map <-- temperature-to-humidity-label almanac-map+

humidity-to-location-label < 'humidity-to-location map:\n'
humidity-to-location-map <-- humidity-to-location-label almanac-map+

almanac-map <- dest-range-start source-range-start range-length
source-range-start <-- [0-9]+ S
dest-range-start <-- [0-9]+ S
range-length <-- [0-9]+ NL

doc <- seeds NL
       NL
       seed-to-soil-map NL
       soil-to-fertilizer-map NL
       fertilizer-to-water-map NL
       water-to-light-map NL
       light-to-temperature-map NL
       temperature-to-humidity-map NL
       humidity-to-location-map
")

;; <mapping> is a record for the a single map range
(define-record-type <mapping>
  (make-mapping src-start dest-start len)
  mapping?
  (src-start mapping-src-start)
  (dest-start mapping-dest-start)
  (len mapping-len))

(define (mapping:src->dest mapping src)
  "finds the destination of a mapping given a source number or returns #f if out of bounds"
  (let* ((len (mapping-len mapping))
        (src-start (mapping-src-start mapping))
        (src-end (+ src-start (mapping-len mapping)))
        (dest-start (mapping-dest-start mapping)))
    (if (and (>= src src-start) (< src src-end))
        (+ dest-start (- src src-start))
        #f)))

(define (sexp->mapping sexp)
  "converts the parsed mapping sexp into a mapping"
  (match-let ((`((dest-range-start ,dest-start)
                 (source-range-start ,src-start)
                 (range-length ,length))
               sexp))
    (make-mapping (string->number src-start)
                  (string->number dest-start)
                  (string->number length))))


(define (mappings->dest mappings src)
  "given a list of mappings, find the first destination or return src"
  (or (any (cut mapping:src->dest <> src) mappings)
      src))

;; <almanac> is a record for the entire almanac input
(define-record-type <almanac>
  (make-almanac seeds
                seed-to-soil
                soil-to-fertilizer
                fertilizer-to-water
                water-to-light
                light-to-temperature
                temperature-to-humidity
                humidity-to-location)
  almanac?
  (seeds almanac-seeds)
  (seed-to-soil almanac-seed-to-soil)
  (soil-to-fertilizer almanac-soil-to-fertilizer)
  (fertilizer-to-water almanac-fertilizer-to-water)
  (water-to-light almanac-water-to-light)
  (light-to-temperature almanac-light-to-temperature)
  (temperature-to-humidity almanac-temperature-to-humidity)
  (humidity-to-location almanac-humidity-to-location))

(define (sexp->almanac sexp)
  "convert the parsed almanac sexp into an almanac record"
  (define sexp->seed
    (match-lambda (('int n) (string->number n))))

  (define (sexp->seeds sexp)
    (map sexp->seed sexp))

  (match-let ((`((seeds . ,seeds)
                 (seed-to-soil-map . ,seed-to-soil)
                 (soil-to-fertilizer-map . ,soil-to-fertilizer)
                 (fertilizer-to-water-map . ,fertilizer-to-water)
                 (water-to-light-map . ,water-to-light)
                 (light-to-temperature-map . ,light-to-temperature)
                 (temperature-to-humidity-map . ,temperature-to-humidity)
                 (humidity-to-location-map . ,humidity-to-location))
               sexp))
    (make-almanac (sexp->seeds seeds)
                  (map sexp->mapping seed-to-soil)
                  (map sexp->mapping soil-to-fertilizer)
                  (map sexp->mapping fertilizer-to-water)
                  (map sexp->mapping water-to-light)
                  (map sexp->mapping light-to-temperature)
                  (map sexp->mapping temperature-to-humidity)
                  (map sexp->mapping humidity-to-location))))

(define (input->almanac input)
  "parse an input string into an almanac"
  (sexp->almanac (peg:tree (match-pattern doc (string-append input "\n")))))

(define (seed->location almanac seed)
  "given a seed value, find its location"
  (define (f getter x) (mappings->dest (getter almanac) x))

  (f almanac-humidity-to-location
     (f almanac-temperature-to-humidity
        (f almanac-light-to-temperature
           (f almanac-water-to-light
              (f almanac-fertilizer-to-water
                 (f almanac-soil-to-fertilizer
                    (f almanac-seed-to-soil seed))))))))

(define (almanac->locations almanac)
  "given an almanac find the locations for all seeds"
  (map (cut seed->location almanac <>) (almanac-seeds almanac)))

;; define tests here so that I can run them interactively
(define (day05-tests)
  (define *input-ex* "seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4")

  (define mapping (make-mapping 50 98 2))
  (define *input* (call-with-input-file "./inputs/day05-input1.txt" get-string-all))
  (define almanac-from-input (input->almanac *input*))
  (define almanac (input->almanac *input-ex*))

  (test-begin "day05")

  (test-eq #f
    (mapping:src->dest mapping 10))

  (test-eq 98
    (mapping:src->dest mapping 50))

  (test-eq 99
    (mapping:src->dest mapping 51))

  (test-eq #f
    (mapping:src->dest mapping 52))


  (test-eq 82 (seed->location almanac 79))

  (test-eq 43 (seed->location almanac 14))

  (test-eq 86 (seed->location almanac 55))

  (test-eq 35 (seed->location almanac 13))

  (test-equal '(82 43 86 35)
    (almanac->locations almanac))


  (test-equal 806029445
    (apply min (almanac->locations almanac-from-input)))

  (test-end "day05"))
