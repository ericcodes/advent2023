(define-module (advent2023 day02)
  #:export (solution-1 solution-2 make-sack))

(use-modules ((ice-9 peg))
             ((ice-9 match))
             ((srfi srfi-1))
             ((srfi srfi-9)))

;; a sack record contains red, green, and blue cube counts
(define-record-type <sack>
  (make-sack red green blue)
  sack?
  (red sack-red)
  (green sack-green)
  (blue sack-blue))

;; sack-contains? is true if s2 can be contained in s1
(define (sack-contains? s1 s2)
  (and
   (>= (sack-red s1) (sack-red s2))
   (>= (sack-green s1) (sack-green s2))
   (>= (sack-blue s1) (sack-blue s2))))

;; round->sack converts a (round (red "1") (green "2") (blue "3")) list into a
;; sack record
(define (round->sack round)
  (define colors (cdr round))
  (fold (lambda (color sack)
          (match color
            ;; match red color
            (`(red ,red)
             (make-sack (string->number red)
                        (sack-green sack)
                        (sack-blue sack)))

            ;; match green color
            (`(green ,green)
             (make-sack (sack-red sack)
                        (string->number green)
                        (sack-blue sack)))

            ;; match blue color
            (`(blue ,blue)
             (make-sack (sack-red sack)
                        (sack-green sack)
                        (string->number blue)))))
        (make-sack 0 0 0)
        colors))

;; Here is our PEG grammer for the input
(define-peg-string-patterns
  "doc <- game* !.
game <-- Game [0-9]+ C round+
round <-- (red/green/blue)+ S? NL*
red <-- [0-9]+ R
green <-- [0-9]+ G
blue <-- [0-9]+ B
NL < '\n'
Game < 'Game '
C < ': '
S < '; '
R < ' red' ', '?
G < ' green' ', '?
B < ' blue' ', '?
")

;; sack-contains-game? returns true if every round in a game can be pulled from a sack
(define (sack-contains-game? sack game)
  (every (lambda (round)
           (sack-contains? sack (round->sack round)))
         (game->rounds game)))

;; solution-1 finds all the games that can be pulled from a sack and sums their ids
(define (solution-1 sack input)
  (define games (peg:tree (match-pattern doc input)))
  (fold (lambda (game acc)
          (if (sack-contains-game? sack game)
              (+ acc (string->number (cadr game)))
              acc))
        0
        games))

;; game->minimum-sack finds a sack that has enough cubes to satisfy all rounds of a game
(define (game->minimum-sack game)
  (fold (lambda (round min-sack)
          (define sack (round->sack round))
          (make-sack (max (sack-red min-sack) (sack-red sack))
                     (max (sack-green min-sack) (sack-green sack))
                     (max (sack-blue min-sack) (sack-blue sack))))
        (make-sack 0 0 0)
        (game->rounds game)))

;; game->rounds returns the rounds list for a given game
(define (game->rounds game) (caddr game))

;; sack->power calculates the power for a given sack
(define (sack->power sack)
  (* (sack-red sack) (sack-green sack) (sack-blue sack)))

;; solution-2 finds all the minimum sack for each games and sums their powers
(define (solution-2 input)
  (define games (peg:tree (match-pattern doc input)))
  (fold (lambda (game acc)
          (+ acc (sack->power (game->minimum-sack game))))
        0
        games))
