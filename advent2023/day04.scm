(define-module (advent2023 day04)
  #:export (solution-1 solution-2 parse-input score-cards))

(use-modules (ice-9 peg)
             (ice-9 match)
             (ice-9 format)
             (srfi srfi-11)
             (srfi srfi-1)
             (srfi srfi-9))

(define-record-type <card>
  (make-card id wins)
  card?
  (id card->id)
  (wins card->wins))


(define-peg-string-patterns
  "NL < '\n'
S < ' '
P < '| '
Card < 'Card' S+
number <-- S* [0-9]* S*
winning <-- number+
picks <-- number+
C < ': '
card-id <-- Card [0-9]+

cards <- card* !.
card <-- card-id C (winning) P (picks) NL*)
")

(define (numbers->set numbers)
  "converts the `(number ,digits) list that the parser produces"
  (define (folder number set)
    (match number
      ((number digits) (cons (string->number digits) set))))

  (fold folder '() numbers))

;; derived from https://github.com/scheme-requests-for-implementation/srfi-1/blob/36acda4744f45fbbbbddab854d78c2d209f992ea/srfi-1-reference.scm#L482-L487
(define (take-at-most lis k)
  (let recur ((lis lis) (k k))
    (if (or (null? lis) (zero? k)) '()
	      (cons (car lis)
	            (recur (cdr lis) (- k 1))))))

(define (card->score card)
  "card->score calculates the score which is doubled for every win"
  (define wins (card->wins card))
  (if (zero? wins)
      0
      (expt 2 (1- wins))))

(define (parse-input input)
  "parse-input converts the PEG tree into our card record"
  (define (mapper parsed)
    (match-let (
                (`(card
                  (card-id ,card-id)
                  (winning . ,winning)
                  (picks . ,picks))
                parsed))
      (let* ((winning (numbers->set winning))
            (picks (numbers->set picks))
            (wins (length (lset-intersection eqv? winning picks))))
      (make-card card-id wins))))

  (map mapper
       (peg:tree (match-pattern cards input))))

(define (solution-1 input)
  "This is solution #1, it uses the double for each win scoring method"
  (define (folder card total)
    (+ total (card->score card)))

  (fold folder 0 (parse-input input)))


(define (score-cards stack full-deck)
  "The stack is the cards that need to be processed. The stack may be truncated because it may be just the cloned cards.
We need the full-deck to know which cards to cloned
i.e. if we have the following cards:

(1 2 3 4 5 6 7)

Lets say card we're processing card 3:

stack = (3 4 5 6 7)
full-deck (3 4 5 6 7)

If it had two wins, The cloned stack would look like:

stack = (4 5)
full-deck  = (4 5 6 7)

If for instance 5 has 3 wins, if I used the stack to clone from, the
clones would be (). So I clone from full-deck to get (6 7)
"
  (if (null? stack)
      0
      (let* ((card (car full-deck))
             (winning-count (card->wins card))
             (clones (take-at-most full-deck winning-count)))
        (+ 1
           (score-cards (cdr stack) (cdr full-deck))
           (score-cards clones (cdr full-deck))))))

(define (solution-2 input)
  (define parsed (parse-input input))
  (score-cards parsed parsed))
