# Eric Codes' Advent of Code 2023 solutions

These are my Advent of Code 2023 solutions. 

## Dependencies

I use `flake.nix` to manage dependencies, but as long as you have the GNU build tools and guile 3.0 you should be able to run the tests

## Run tests

```sh
./configure && make check
```
